<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/bookings', 'Api\BookingController@index');
Route::post('/bookings', 'Api\BookingController@store');
Route::post('/bookings/{booking}/accept', 'Api\BookingController@accept');
Route::post('/bookings/{booking}/cancel', 'Api\BookingController@cancel');
Route::post('/bookings/{booking}', 'Api\BookingController@update');
Route::get('/bookings/{booking}', 'Api\BookingController@show');
Route::delete('/bookings/{booking}', 'Api\BookingController@destroy');

