<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Booking as BookingResource;
use App\Models\Booking;
use App\User;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  DB::table('bookings')
                     ->join('users', 'bookings.user_id','=', 'users.id')
                     ->join('car_types', 'bookings.car_type_id','=', 'car_types.id')
                     ->select('bookings.id', 'users.name', 'users.email', 'car_types.type', 'bookings.status')
                     ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $booking = new Booking();
        $booking->user_id = User::where('email',$request->email)->first()->id;
        $booking->car_type_id = $request->car_type_id;
        $booking->status = 'PENDING';
        $booking->save();
        
        return new BookingResource($booking);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new BookingResource(Booking::FindOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //I am doing validation here
        $booking = Booking::findOrFail($id);
        $booking->user_id = User::where('email',$request->email)->first()->id;
        $booking->car_type_id = $request->car_type_id;
        $booking->status = $request->status;
        $booking->save();
        
        return new BookingResource($booking);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function accept(Request $request, $id)
    {
        //I am doing validation here
        $booking = Booking::findOrFail($id);
        $booking->status = 'ACCEPTED';
        $booking->save();
        
        return new BookingResource($booking);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request, $id)
    {
        //I am doing validation here
        $booking = Booking::findOrFail($id);
        $booking->status = 'CANCELED';
        $booking->save();
        
        return new BookingResource($booking);
    }    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = Booking::findOrFail($id);
        $booking->delete();
    }
}
