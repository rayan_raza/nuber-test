<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    /**
     * success response method.
     *
     * @param null $result
     * @param null $message
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($result = null, $message = null, $code = 200)
    {
        $response = [];
        if ($result) {
            $response['data'] = $result;
        }
        if ($message) {
            $response['message'] = $message;
        }
        return response()->json($response, $code);
    }

    /**
     * return error response.
     *
     * @param array $error
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendError($error = [], $code = 404)
    {
        $response = [
            'error' => $error,
        ];
        return response()->json($response, $code);
    }
}
